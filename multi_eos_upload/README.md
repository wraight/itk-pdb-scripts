# Multi EOS Uploads

In the form of notebooks

## eos_delete notebook

Test EOS deletions

- get object
- upload attachment
- delete attachment


## eos_testRun_ids notebooks

Get testRun ids from component population and testType

- set filters
- loop over matching components
- get matching testRun ids 


## eos_looper notebook

Script for Multiple EOS Uploads

- define json
    - object identifier (component / testRun)
    - define list of image paths per object
- loop over object identifier 
- attach images 

